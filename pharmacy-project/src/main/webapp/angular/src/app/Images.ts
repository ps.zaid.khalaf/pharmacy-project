export class Images {
    id: number;
    imageName: string;
    type: string;
    picByte: string;
}
