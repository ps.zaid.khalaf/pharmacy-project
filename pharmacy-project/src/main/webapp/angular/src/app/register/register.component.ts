import {Component, OnInit} from '@angular/core';
import {User} from "../User";
import {Router} from "@angular/router";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  public user: User = new User();
  submitted = false;


  constructor(private userService: UserService, private router: Router) {

  }

  ngOnInit(): void {
  }

  save() {
    this.userService.createUser(this.user).subscribe(data => {
        this.user = new User();
        this.gotoCreditForm();
      }, err => {
        window.alert("didn't create user something went wrong");
        this.router.navigate(['/add']);
      }
    );
  }

  onCreateSubmit() {
    this.submitted = true;
    this.save();
  }

  gotoCreditForm() {
    this.router.navigate(['creditInfo']);
  }

}
