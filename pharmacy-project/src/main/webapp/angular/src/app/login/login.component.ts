import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthenticationService} from "../services/authentication.service";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  username: String;
  password: String;
  invalidLogin = false

  constructor(private router: Router,
              private loginservice: AuthenticationService, private userService: UserService) {
  }

  ngOnInit() {

  }

  checkLogin() {
    (this.loginservice.authenticate(this.username, this.password).subscribe(
        data => {
          window.confirm("success!! to the home page");
          this.router.navigate(['/home'])
          this.invalidLogin = false;
        },
        error => {
          this.invalidLogin = true
        }
      )
    );

  }


}
