import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginComponent} from './login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {HomeComponent} from './home/home.component';
import {RouterModule} from "@angular/router";
import {RegisterComponent} from './register/register.component';
import {LogoutComponent} from './logout/logout.component';
import {GallaryComponent} from "./gallary/gallary.component";
import {ContactUsComponent} from "./contact-us/contact-us.component";
import {BuydrugsComponent} from './buydrugs/buydrugs.component';
import {NoopAnimationsModule} from '@angular/platform-browser/animations';
import {ManagementComponent} from './management/management.component';
import {MatTabsModule} from "@angular/material/tabs";
import {MatTableModule} from "@angular/material/table";
import {DurgDetailComponent} from './durg-detail/durg-detail.component';
import {AddphotoComponent} from './addphoto/addphoto.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { CreditInfoComponent } from './credit-info/credit-info.component';
import { TeamComponent } from './team/team.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    RegisterComponent,
    LogoutComponent,
    GallaryComponent,
    ContactUsComponent,
    BuydrugsComponent,
    ManagementComponent,
    DurgDetailComponent,
    AddphotoComponent,
    CreditInfoComponent,
    TeamComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    NoopAnimationsModule,
    MatTabsModule,
    MatTableModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
