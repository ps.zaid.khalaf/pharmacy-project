import {Component, OnInit} from '@angular/core';
import {CreditService} from "../services/credit.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Credit} from "../credit";

@Component({
  selector: 'app-credit-info',
  templateUrl: './credit-info.component.html',
  styleUrls: ['./credit-info.component.css']
})
export class CreditInfoComponent implements OnInit {
  credit: Credit = new Credit();
  public email: string;

  constructor(private creditService: CreditService, private router: Router, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
  }

  onCreateSubmit() {
    this.save();
  }

  save() {
    this.creditService.addCredit(this.credit).subscribe(data => {
        this.credit = new Credit();
        window.confirm("credit info updated");
      }, err => {
        window.confirm("something went wrong");
      }
    );
  }
}
