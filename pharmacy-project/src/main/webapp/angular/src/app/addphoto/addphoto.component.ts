import {Component, OnInit} from '@angular/core';
import {HttpClient, HttpHandler} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {Drug} from "../Drug";
import {catchError, map} from "rxjs/operators";
import {Images} from "../Images";

@Component({
  selector: 'app-addphoto',
  templateUrl: './addphoto.component.html',
  styleUrls: ['./addphoto.component.css']
})
export class AddphotoComponent implements OnInit {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  ngOnInit(): void {
  }

  selectedFile: File;
  retrievedImage: any;
  base64Data: any;
  retrieveResonse: any;
  message: string;
  imageName: any;  //Gets called when the user selects an image
  drugName: string;

  public onFileChanged(event) {
    //Select File
    this.selectedFile = event.target.files[0];
  }

  //Gets called when the user clicks on submit to upload the image
  onUpload() {
    console.log(this.selectedFile);
    //FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile, this.drugName);

    //Make a call to the Spring Boot Application to save the image
    this.httpClient.post('http://localhost:8080/image/upload', uploadImageData, {observe: `response`})
      .subscribe((response) => {
          if (response.status == 200) {
            this.message = 'Image uploaded successfully';
            this.router.navigate(["management"]);
          } else {
            this.message = 'Image not uploaded successfully';
          }
        }
      );
    this.router.navigate(['management']);
  }

  //Gets called when the user clicks on retieve image button to get the image from back end

  getImage() {
    //Make a call to Sprinf Boot to get the Image Bytes.
    this.httpClient.get('http://localhost:8080/image/get/' + this.imageName)
      .subscribe(
        res => {
          this.retrieveResonse = res;
          this.base64Data = this.retrieveResonse.picByte;
          this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
        }
      );
  }

}
