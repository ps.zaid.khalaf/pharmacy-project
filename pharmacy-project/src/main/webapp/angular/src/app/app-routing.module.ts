import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {HomeComponent} from "./home/home.component";
import {HTTP_INTERCEPTORS} from "@angular/common/http";
import {AuthHttpInterceptorService} from "./services/auth-http-interceptor.service";
import {RegisterComponent} from "./register/register.component";
import {AuthGuardService} from "./services/auth-guard.service";
import {LogoutComponent} from "./logout/logout.component";
import {GallaryComponent} from "./gallary/gallary.component";
import {ContactUsComponent} from "./contact-us/contact-us.component";
import {BuydrugsComponent} from "./buydrugs/buydrugs.component";
import {ManagementComponent} from "./management/management.component";
import {DurgDetailComponent} from "./durg-detail/durg-detail.component";
import {AddphotoComponent} from "./addphoto/addphoto.component";
import {CreditInfoComponent} from "./credit-info/credit-info.component";
import {TeamComponent} from "./team/team.component";

const routes: Routes = [{path: 'login', component: LoginComponent}, {
  path: 'logout',
  component: LogoutComponent,
  canActivate: [AuthGuardService]
},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuardService]}, {
    path: 'register', component: RegisterComponent
  },
  {path: 'gallary', component: GallaryComponent}, {path: 'contact-us', component: ContactUsComponent},
  {path: 'drugs', component: BuydrugsComponent, canActivate: [AuthGuardService]},
  {path: 'management', component: ManagementComponent, canActivate: [AuthGuardService]},
  {path: 'drug-details/:name', component: DurgDetailComponent, canActivate: [AuthGuardService]}
  , {path: 'upload', component: AddphotoComponent, canActivate: [AuthGuardService]},
  {path: 'creditInfo', component: CreditInfoComponent}, {path: 'team', component: TeamComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS, useClass: AuthHttpInterceptorService, multi: true
    }
  ],
})
export class AppRoutingModule {
}
