import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {Observable, throwError} from "rxjs";
import {Drug} from "../Drug";
import {catchError} from "rxjs/operators";


@Injectable({
  providedIn: 'root'
})
export class DrugService {
  private commentServiceUrl = "http://localhost:8080/api/drug";

  constructor(private httpClient: HttpClient) {
  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!' + error.status + "message " + error.message;
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else if (error.status == 500) {
      errorMessage = `Error Code: ${error.status}\n Message: internal server error something went wrong` + "error message " + error.message;
    } else if (error.status == 406) {
      errorMessage = `Error Code: ${error.status}\n Message: the product you wish to buy is out of stock`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  addDrug(drug: Drug) {
    return this.httpClient.post(this.commentServiceUrl, drug).pipe(catchError(this.handleError));
  }

  getAllDrugs(): Observable<Array<Drug>> {
    return this.httpClient.get<Array<Drug>>(this.commentServiceUrl + "/all").pipe(catchError(this.handleError));
  }

  decreaseQuantity(drugName: String) {
    return this.httpClient.put(this.commentServiceUrl + '/decrease/' + drugName, {drugName: drugName}).pipe(catchError(this.handleError));

  }

  deleteDrug(name: String) {
    return this.httpClient.delete(`${this.commentServiceUrl}` + '/delete/' + name).pipe(catchError(this.handleError));
  }

  getDrugByName(name: String) {
    return this.httpClient.get<Drug>(this.commentServiceUrl + '/info/' + name).pipe(catchError(this.handleError));
  }

  updateDrug(name: String, value: any): Observable<Object> {
    return this.httpClient.put(`${this.commentServiceUrl}` + '/update/' + name, value);
  }

}
