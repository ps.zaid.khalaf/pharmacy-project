import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Images} from "../Images";
import {Observable, Subscription} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class ImageService {
  public fromApi: Subscription;
  private retrieveResonse: any;
  private base64Data: any;
  private retrievedImage: string;

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  selectedFile: File;
  message: string;

  ngOnInit(): void {
  }

  onUpload(drugName: string) {
    console.log(this.selectedFile);
    //FormData API provides methods and properties to allow us easily prepare form data to be sent with POST HTTP requests.
    const uploadImageData = new FormData();
    uploadImageData.append('imageFile', this.selectedFile, drugName);

    //Make a call to the Spring Boot Application to save the image
    this.httpClient.post('http://localhost:8080/image/upload', uploadImageData, {observe: `response`})
      .subscribe((response) => {
          if (response.status === 200) {
            this.message = 'Image uploaded successfully';
          } else {
            this.message = 'Image not uploaded successfully';
          }
        }
      );
    this.router.navigate(['management']);
  }

  deleteImage(drugName: String) {
    return this.httpClient.delete('http://localhost:8080/image/delete/' + drugName);
  }

  formatImage(res: any) {
    this.base64Data = res;
    return this.retrievedImage = 'data:image/jpeg;base64,' + this.base64Data;
  }

  public onFileChanged(event) {
    this.selectedFile = event.target.files[0];
  }

}
