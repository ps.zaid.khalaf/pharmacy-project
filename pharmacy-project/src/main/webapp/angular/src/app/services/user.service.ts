import {Injectable} from '@angular/core';
import {Observable, throwError} from "rxjs";
import {catchError} from "rxjs/operators";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {User} from 'src/app/User';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  private baseUrl = 'http://localhost:8080/api/users';
  private registerUrl = 'http://localhost:8080/register';
  private user: Observable<User>;

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error :' + error.status + "  message: " + error.message;
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else if (error.status == 400) {
      errorMessage = "user already exist";
    } else if (error.status == 500) {
      errorMessage = `Error Code: ${error.status}\nMessage: internal server error something went wrong`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  constructor(private http: HttpClient) {
  }

  createUser(user: Object): Observable<Object> {
    return this.http.post(`${this.registerUrl}`, user).pipe(catchError(this.handleError));

  }

  deleteUserByName(name: String) {
    return this.http.delete(`${this.baseUrl}` + '/delete/'+name).pipe(catchError(this.handleError));
  }

  getUsersList():Observable<any> {
   return  this.http.get(`${this.baseUrl}`).pipe(catchError(this.handleError));
  }

  getUserByName(name: String): Observable<User> {
    return this.http.get<User>(this.baseUrl + "/info/" + name).pipe(catchError(this.handleError));
  }















  isAuthorized(name: String): Boolean {
    this.user = this.getUserByName(name);
    this.user.subscribe(data => {
      if (data.admin) {
        sessionStorage.setItem("isAdmin", String(data.admin));
      }
    }, error => {
    });
    return false
  }
}
