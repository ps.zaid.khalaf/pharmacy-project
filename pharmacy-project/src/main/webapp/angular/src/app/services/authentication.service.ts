import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {map} from "rxjs/operators";
import {UserService} from "./user.service";


@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private httpClient: HttpClient, private userService: UserService) {
  }

  authenticate(userName, password) {

    return this.httpClient.post<any>('http://localhost:8080/authenticate', {userName, password}).pipe(
      map(
        userData => {
          sessionStorage.setItem('username', userName);
          let tokenStr = 'Bearer ' + userData.token;
          sessionStorage.setItem('token', tokenStr);
          this.userService.isAuthorized(userName);
          return userData;
        }
      )
    );
  }

  isUserAdmin() {
    let isAdmin = sessionStorage.getItem("isAdmin");
    console.log(!(isAdmin === null))
    return (isAdmin === "true")
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('username')
    console.log(!(user === null))
    return !(user === null)
  }

  logOut() {
    sessionStorage.removeItem('username');
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('isAdmin');
  }

}
