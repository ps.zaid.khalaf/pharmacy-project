import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {catchError} from "rxjs/operators";
import {throwError} from "rxjs";
import {Credit} from "../credit";

@Injectable({
  providedIn: 'root'
})
export class CreditService {
  baseUrl = "http://localhost:8080/api/credit";


  constructor(private http: HttpClient) {

  }

  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error :' + error.status + "  message: " + error.message;
    if (error.error instanceof ErrorEvent) {
      errorMessage = `Error: ${error.error.message}`;
    } else if (error.status == 400) {
      errorMessage = "user already exist";
    } else if (error.status == 500) {
      errorMessage = `Error Code: ${error.status}\nMessage: internal server error something went wrong`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  addCredit(credit: Credit) {
    return this.http.post(this.baseUrl, credit).pipe(catchError(this.handleError));
  }

  deleteUserByName(email: String) {
    return this.http.delete(`${this.baseUrl}` + '/delete/' + email).pipe(catchError(this.handleError));
  }
}
