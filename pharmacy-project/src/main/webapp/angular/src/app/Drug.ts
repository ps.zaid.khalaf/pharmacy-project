export class Drug {
  id: number;
  drugName: string;
  price: number;
  quantity: number;
  picByte: any;
}
