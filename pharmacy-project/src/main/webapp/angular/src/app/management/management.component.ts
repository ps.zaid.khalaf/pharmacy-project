import {Component, OnInit} from '@angular/core';
import {MatTabsModule} from '@angular/material/tabs';
import {UserService} from "../services/user.service";
import {DrugService} from "../services/drug.service";
import {Observable} from "rxjs";
import {User} from "../User";
import {Drug} from "../Drug";
import {Router} from "@angular/router";
import {ImageService} from "../services/image.service";

@Component({
  selector: 'app-management',
  templateUrl: './management.component.html',
  styleUrls: ['./management.component.css']
})
export class ManagementComponent implements OnInit {
  users: Observable<User[]>;
  drugs: Observable<Drug[]>;
  drug: Drug = new Drug();

  constructor(private userService: UserService, private drugService: DrugService, private router: Router, private imageService: ImageService) {
  }

  ngOnInit(): void {
    this.reloadData();
  }

  reloadData() {
    this.reloadUsers();
    this.reloadDrugs();
  }

  deleteUser(name: String) {
    this.userService.deleteUserByName(name).subscribe(data => {
        window.confirm("user was deleted");
        this.reloadUsers();
      }, error => {
        window.alert("something went wrong");
        console.log(error);
      }
    );
  }

  deleteDrug(name: String) {
    this.imageService.deleteImage(name);
    this.drugService.deleteDrug(name).subscribe(data => {
        window.confirm("drug was was deleted");
        this.reloadDrugs()
      }, error => {
        window.alert("something went wrong");
        console.log(error);
      }
    );
  }

  drugDetail(name: String) {
    this.router.navigate(['drug-details', name]);

  }

  reloadUsers() {
    this.users = this.userService.getUsersList();
  }

  reloadDrugs() {
    this.drugs = this.drugService.getAllDrugs();
  }


  onCreateSubmit() {
    this.save();
  }


  save() {
    this.drugService.addDrug(this.drug).subscribe(data => {
        this.drug = new Drug();
        this.back();
        this.router.navigate(['/upload']);
      }, err => {
        window.confirm("something went wrong");
        this.router.navigate(['/add']);
      }
    );
  }

  private back() {
    this.router.navigate(['/management']);
  }

  onUpload(drugName: string) {
    this.imageService.onUpload(drugName);
  }

  onFileChanged($event: Event) {
    this.imageService.onFileChanged($event);
  }
}
