import { Component, OnInit } from '@angular/core';
import {DrugService} from "../services/drug.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Drug} from "../Drug";
import {HttpErrorResponse} from "@angular/common/http";
import {throwError} from "rxjs";

@Component({
  selector: 'app-durg-detail',
  templateUrl: './durg-detail.component.html',
  styleUrls: ['./durg-detail.component.css']
})
export class DurgDetailComponent implements OnInit {
  name: String;
  drug: Drug;
  constructor(private route: ActivatedRoute,private drugService:DrugService,private router:Router) {

  }

  ngOnInit(): void {
    this.drug = new Drug();
    this.name = this.route.snapshot.params['name'];
    this.drugService.getDrugByName(this.name).subscribe(data => {
      console.log(data)

      this.drug = data;
    }, error => console.log(error));
  }

  gotoList() {
    this.router.navigate(['/management']);
  }
  onSubmit() {
    this.drugService.updateDrug(this.name, this.drug)
      .subscribe(data => {
        console.log(data);
        this.drug = new Drug();
        this.gotoList();
      }, error => console.log(error));

  }
}
