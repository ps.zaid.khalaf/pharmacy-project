import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DurgDetailComponent } from './durg-detail.component';

describe('DurgDetailComponent', () => {
  let component: DurgDetailComponent;
  let fixture: ComponentFixture<DurgDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DurgDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DurgDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
