import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BuydrugsComponent } from './buydrugs.component';

describe('BuydrugsComponent', () => {
  let component: BuydrugsComponent;
  let fixture: ComponentFixture<BuydrugsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BuydrugsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BuydrugsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
