import {Component, OnInit} from '@angular/core';
import {DrugService} from "../services/drug.service";
import {Drug} from "../Drug";
import {Observable} from "rxjs";
import {ImageService} from "../services/image.service";
import {Router} from "@angular/router";
import {Images} from "../Images";
import {DomSanitizer} from "@angular/platform-browser";

@Component({
  selector: 'app-buydrugs',
  templateUrl: './buydrugs.component.html',
  styleUrls: ['./buydrugs.component.css']
})
export class BuydrugsComponent implements OnInit {
  path: string = "src/main/webapp/angular/src/images";
  allDrugs: Observable<Array<Drug>>;
  public allImages: Observable<Array<Images>>;

  constructor(private drugService: DrugService, private imageService: ImageService, private router: Router, private sanitizer: DomSanitizer) {
  }

  ngOnInit(): void {
    this.allDrugs = this.drugService.getAllDrugs();
  }


  decreaseQuantity(drugName: string) {
    this.drugService.decreaseQuantity(drugName).subscribe(data => {
        window.confirm("drug bought successfully");
      }, error => {
        window.alert("something went wrong");
      }
    );
  }

  formatImage(image: any) {
    return this.imageService.formatImage(image);
  }

  goToCredit(drugName: string) {
    this.drugService.decreaseQuantity(drugName).subscribe(data => {
        this.router.navigate(['/creditInfo']);
      }, error => {
        window.alert("something went wrong");
      }
    );
  }
}
