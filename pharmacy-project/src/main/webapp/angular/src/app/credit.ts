export class Credit {
  email: string
  address: string
  creditNumber: string
  city: string
  expirationMonth: string
  expirationYear: string
  phoneNumber: string
  cvv: string
  governorate: string
}
