package com.gradProject.pharmacyproject.gradproject.entities.implments;

import javax.persistence.*;

@Entity
@Table(name = "crdt_info")
public class CreditEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "email")
    private String email;
    @Column(name = "address")
    private String address;
    @Column(name = "crdt_nmbr")
    private String creditNumber;
    @Column(name = "Governorate")
    private String governorate;
    @Column(name = "city")
    private String city;
    @Column(name = "exp_mnth")
    private String expirationMonth;
    @Column(name = "exp_year")
    private String expirationYear;
    @Column(name = "phon_nmbr")
    private String phoneNumber;
    @Column(name = "cvv")
    private String cvv;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreditNumber() {
        return creditNumber;
    }

    public void setCreditNumber(String creditNumber) {
        this.creditNumber = creditNumber;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getExpirationMonth() {
        return expirationMonth;
    }

    public void setExpirationMonth(String expirationMonth) {
        this.expirationMonth = expirationMonth;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String zipCode) {
        this.phoneNumber = zipCode;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public String getGovernorate() {
        return governorate;
    }

    public void setGovernorate(String governorate) {
        this.governorate = governorate;
    }

    public String getExpirationYear() {
        return expirationYear;
    }

    public void setExpirationYear(String expirationYear) {
        this.expirationYear = expirationYear;
    }
}
