package com.gradProject.pharmacyproject.gradproject.repositry;

import com.gradProject.pharmacyproject.gradproject.entities.implments.ImageEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ImageRepository extends JpaRepository<ImageEntity, Integer> {

    Optional<ImageEntity> findByName(String name);

    void deleteByName(String name);

    boolean existsByName(String name);
}
