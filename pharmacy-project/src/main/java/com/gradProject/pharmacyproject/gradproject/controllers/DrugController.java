package com.gradProject.pharmacyproject.gradproject.controllers;

import com.gradProject.pharmacyproject.gradproject.dto.DrugDto;
import com.gradProject.pharmacyproject.gradproject.services.DrugService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/drug")
public class DrugController {

    @Autowired
    DrugService drugService;

    @PostMapping
    public ResponseEntity saveDrug(@RequestBody DrugDto dto) {
        return drugService.createDrug(dto);
    }

    @GetMapping("info/{drugName}")
    public ResponseEntity<DrugDto> getDrugByName(@PathVariable(value = "drugName") String drugByName) {
        return drugService.getDrugByName(drugByName);
    }

    @GetMapping("all")
    public List<DrugDto> getAllDrugs() {
        return drugService.getAllDrugs();
    }

    @DeleteMapping("/delete/{drugName}")
    @Transactional
    public ResponseEntity deleteUser(@PathVariable(value = "drugName") String drugName) {
        return drugService.deleteDrugByName(drugName);
    }

    @PutMapping("/decrease/{drugName}")
    public ResponseEntity decreaseQuantity(@PathVariable @RequestBody String drugName) {
        return drugService.decreaseQuantity(drugName);
    }
    @PutMapping("/update/{name}")
    public ResponseEntity<DrugDto> updateUser(@PathVariable(value = "name") String name, @RequestBody DrugDto sysUserDetails) {
        return drugService.updateDrug(name, sysUserDetails);
    }


}
