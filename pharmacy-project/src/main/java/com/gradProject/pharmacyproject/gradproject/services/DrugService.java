package com.gradProject.pharmacyproject.gradproject.services;

import com.gradProject.pharmacyproject.gradproject.dto.DrugDto;
import com.gradProject.pharmacyproject.gradproject.entities.implments.DrugEntity;
import com.gradProject.pharmacyproject.gradproject.mapping.DrugMapping;
import com.gradProject.pharmacyproject.gradproject.repositry.DrugRepository;
import com.gradProject.pharmacyproject.gradproject.repositry.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DrugService {
    @Autowired
    private DrugRepository drugRepository;

    @Autowired
    private ImageRepository imageRepository;


    public ResponseEntity createDrug(DrugDto dto) {
        if (dto.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        DrugEntity drugEntity = DrugMapping.fromDtoToEntity(dto);
        drugRepository.save(drugEntity);
        return new ResponseEntity(HttpStatus.OK);
    }

    public ResponseEntity<DrugDto> getDrugByName(String drugName) {
        if (drugName.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        DrugDto sysUser = DrugMapping.
                fromEntityToDto(drugRepository.getByDrugName(drugName));
        return ResponseEntity.ok().body(sysUser);
    }

    public ResponseEntity deleteDrugByName(String drugName) {
        if (drugName.isEmpty()) {
            return ResponseEntity.badRequest().build();
        }
        drugRepository.deleteByDrugName(drugName);
        return ResponseEntity.ok().build();
    }

    public List<DrugDto> getAllDrugs() {
        List<DrugDto> dtoList = DrugMapping.fromEntityListToDtoList(drugRepository.findAll());
        for (DrugDto dto : dtoList) {
            if (imageRepository.existsByName(dto.getDrugName())) {
                dto.setPicByte(ImageService.decompressBytes(imageRepository.findByName(dto.getDrugName()).get().getPicByte()));
            }
        }
        return dtoList;
    }

    @Transactional
    public ResponseEntity<DrugDto> decreaseQuantity(String drugName) {
        Optional<DrugEntity> studentOptional = drugRepository.findByDrugName(drugName);
        if (!studentOptional.isPresent())
            return ResponseEntity.notFound().build();
        if (studentOptional.get().getQuantity() <= 0)
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        studentOptional.get().setQuantity(studentOptional.get().getQuantity() - 1);
        drugRepository.save(studentOptional.get());
        return ResponseEntity.ok().build();
    }

    public ResponseEntity<DrugDto> updateDrug(String id, DrugDto drugDto) {
        Optional<DrugEntity> studentOptional = drugRepository.findByDrugName(id);
        if (!studentOptional.isPresent())
            return ResponseEntity.notFound().build();
        drugDto.setId(drugDto.getId());
        DrugEntity drugEntity = DrugMapping.fromDtoToEntity(drugDto);
        drugRepository.save(drugEntity);
        return ResponseEntity.ok().build();
    }

}
