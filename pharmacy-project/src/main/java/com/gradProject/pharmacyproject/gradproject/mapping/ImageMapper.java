package com.gradProject.pharmacyproject.gradproject.mapping;

import com.gradProject.pharmacyproject.gradproject.dto.ImageDto;
import com.gradProject.pharmacyproject.gradproject.entities.implments.ImageEntity;

import java.util.ArrayList;
import java.util.List;

public class ImageMapper {

    public static ImageDto fromEntityToDto(ImageEntity entity) {
        ImageDto imageDto = new ImageDto();
        imageDto.setId(entity.getId());
        imageDto.setName(entity.getName());
        imageDto.setType(entity.getType());
        imageDto.setPicByte(entity.getPicByte());
        return imageDto;
    }

    public static ImageEntity fromDtoToEntity(ImageDto imageDto) {
        ImageEntity imageEntity = new ImageEntity();
        imageEntity.setId(imageDto.getId());
        imageEntity.setName(imageDto.getName());
        imageEntity.setType(imageDto.getType());
        imageEntity.setPicByte(imageDto.getPicByte());
        return imageEntity;
    }

    public static List<ImageDto> mapFromEntityListToDtoList(List<ImageEntity> imageEntities) {
        List<ImageDto> imageDtos = new ArrayList<>();
        for (ImageEntity entity : imageEntities) {
            imageDtos.add(fromEntityToDto(entity));
        }
        return imageDtos;
    }
}
