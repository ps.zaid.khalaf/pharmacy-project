package com.gradProject.pharmacyproject.gradproject.controllers;

import com.gradProject.pharmacyproject.gradproject.dto.UserDto;
import com.gradProject.pharmacyproject.gradproject.services.UserService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/users")
public class UserController {
    final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/info/{name}")
    public ResponseEntity<UserDto> getUserByName(@PathVariable(value = "name") String name) {
        return userService.getUserByName(name);
    }

    @DeleteMapping("/delete/{name}")
    public ResponseEntity deleteUser(@PathVariable(value = "name") String userName) {
        return userService.deleteUser(userName);
    }

    @GetMapping
    public List<UserDto> getAllUsers() {

        return userService.getAllUsers();
    }
}
