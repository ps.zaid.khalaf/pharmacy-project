package com.gradProject.pharmacyproject.gradproject.mapping;

import com.gradProject.pharmacyproject.gradproject.dto.CreditDto;
import com.gradProject.pharmacyproject.gradproject.entities.implments.CreditEntity;

public class CreditMapper {

    public static CreditDto fromEntityToDto(CreditEntity entity) {
        CreditDto dto = new CreditDto();
        dto.setId(entity.getId());
        dto.setEmail(entity.getEmail());
        dto.setAddress(entity.getAddress());
        dto.setCreditNumber(entity.getCreditNumber());
        dto.setGovernorate(entity.getGovernorate());
        dto.setCity(entity.getCity());
        dto.setExpirationMonth(entity.getExpirationMonth());
        dto.setExpirationYear(entity.getExpirationYear());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setCvv(entity.getCvv());
        return dto;
    }


    public static CreditEntity fromDtoToEntity(CreditDto creditDto) {
        CreditEntity creditEntity = new CreditEntity();
        creditEntity.setId(creditDto.getId());
        creditEntity.setEmail(creditDto.getEmail());
        creditEntity.setAddress(creditDto.getAddress());
        creditEntity.setCreditNumber(creditDto.getCreditNumber());
        creditEntity.setGovernorate(creditDto.getGovernorate());
        creditEntity.setCity(creditDto.getCity());
        creditEntity.setExpirationMonth(creditDto.getExpirationMonth());
        creditEntity.setExpirationYear(creditDto.getExpirationYear());
        creditEntity.setPhoneNumber(creditDto.getPhoneNumber());
        creditEntity.setCvv(creditDto.getCvv());
        return creditEntity;
    }
}
