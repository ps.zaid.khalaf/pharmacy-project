package com.gradProject.pharmacyproject.gradproject.repositry;

import com.gradProject.pharmacyproject.gradproject.entities.implments.CreditEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CreditRepository extends JpaRepository<CreditEntity, Integer> {
    void deleteByEmail(String email);

    boolean existsByEmail(String email);

}
