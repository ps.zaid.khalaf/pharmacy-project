package com.gradProject.pharmacyproject.gradproject.services;

import com.gradProject.pharmacyproject.gradproject.dto.CreditDto;
import com.gradProject.pharmacyproject.gradproject.mapping.CreditMapper;
import com.gradProject.pharmacyproject.gradproject.repositry.CreditRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class CreditService {
    @Autowired
    CreditRepository creditRepository;


    public ResponseEntity<String> addCredit(CreditDto creditDto) {
        if (Objects.isNull(creditDto)) {
            return ResponseEntity.badRequest().build();
        }
        creditRepository.save(CreditMapper.fromDtoToEntity(creditDto));

        return ResponseEntity.ok().build();
    }

    public ResponseEntity<String> deleteCredit(String email) {
        if (email.equals("")) {
            return ResponseEntity.badRequest().build();
        }
        creditRepository.deleteByEmail(email);
        return ResponseEntity.ok().build();
    }


    public boolean isExist(String email) {
        return creditRepository.existsByEmail(email);
    }
}
