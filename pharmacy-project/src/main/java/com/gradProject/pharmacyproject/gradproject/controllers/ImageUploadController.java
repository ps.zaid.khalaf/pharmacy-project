package com.gradProject.pharmacyproject.gradproject.controllers;

import com.gradProject.pharmacyproject.gradproject.dto.ImageDto;
import com.gradProject.pharmacyproject.gradproject.entities.implments.ImageEntity;
import com.gradProject.pharmacyproject.gradproject.mapping.ImageMapper;
import com.gradProject.pharmacyproject.gradproject.repositry.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.ResponseEntity.BodyBuilder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.w3c.dom.stylesheets.LinkStyle;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.zip.DataFormatException;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping(path = "/image")
public class ImageUploadController {
    @Autowired
    ImageRepository imageRepository;

    @PostMapping("/upload")
    public BodyBuilder uploadImage(@RequestParam("imageFile") MultipartFile file) throws IOException {
        System.out.println("Original Image Byte Size - " + file.getBytes().length);
        ImageEntity img = new ImageEntity(file.getOriginalFilename(), file.getContentType(),
                compressBytes(file.getBytes()));
        imageRepository.save(img);
        return ResponseEntity.status(HttpStatus.OK);
    }

    @GetMapping(path = {"/get/{imageName}"})
    public ImageDto getImage(@PathVariable("imageName") String imageName) {
        final Optional<ImageEntity> retrievedImage = imageRepository.findByName(imageName);
        return ImageMapper.fromEntityToDto(new ImageEntity(retrievedImage.get().getName(), retrievedImage.get().getType(),
                decompressBytes(retrievedImage.get().getPicByte())));
    }    // compress the image bytes before storing it in the database

    @DeleteMapping("/delete/{imageName}")
    @Transactional
    public ResponseEntity<Void> deleteUser(@PathVariable(value = "imageName") String drugName) {
        imageRepository.deleteByName(drugName);
        return ResponseEntity.ok().build();
    }

    @GetMapping("/all")
    public List<ImageDto> getAllImges() {
        return ImageMapper.mapFromEntityListToDtoList(imageRepository.findAll());
    }

    public static byte[] compressBytes(byte[] data) {
        Deflater deflater = new Deflater();
        deflater.setInput(data);
        deflater.finish();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        while (!deflater.finished()) {
            int count = deflater.deflate(buffer);
            outputStream.write(buffer, 0, count);
        }
        try {
            outputStream.close();
        } catch (IOException e) {
            throw new RuntimeException(e.getCause());
        }
        System.out.println("Compressed Image Byte Size - " + outputStream.toByteArray().length);
        return outputStream.toByteArray();
    }
    // uncompress the image bytes before returning it to the angular application

    public static byte[] decompressBytes(byte[] data) {
        Inflater inflater = new Inflater();
        inflater.setInput(data);
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream(data.length);
        byte[] buffer = new byte[1024];
        try {
            while (!inflater.finished()) {
                int count = inflater.inflate(buffer);
                outputStream.write(buffer, 0, count);
            }
            outputStream.close();
        } catch (IOException | DataFormatException ioe) {
            throw new RuntimeException(ioe.getCause());
        }
        return outputStream.toByteArray();
    }
}
