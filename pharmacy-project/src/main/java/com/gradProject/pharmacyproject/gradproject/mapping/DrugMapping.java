package com.gradProject.pharmacyproject.gradproject.mapping;

import com.gradProject.pharmacyproject.gradproject.dto.DrugDto;
import com.gradProject.pharmacyproject.gradproject.entities.implments.DrugEntity;

import java.util.ArrayList;
import java.util.List;

public class DrugMapping {

    public static DrugDto fromEntityToDto(DrugEntity entity) {
        DrugDto dto = new DrugDto();
        dto.setDrugName(entity.getDrugName());
        dto.setPrice(entity.getPrice());
        dto.setId(entity.getId());
        dto.setQuantity(entity.getQuantity());
        return dto;
    }

    public static DrugEntity fromDtoToEntity(DrugDto dto) {
        DrugEntity entity = new DrugEntity();
        entity.setDrugName(dto.getDrugName());
        entity.setPrice(dto.getPrice());
        entity.setId(dto.getId());
        entity.setQuantity(dto.getQuantity());

        return entity;
    }

    public static List<DrugDto> fromEntityListToDtoList(List<DrugEntity> entities) {
        List<DrugDto> dtoList = new ArrayList<>();
        for (DrugEntity entity : entities) {
            DrugDto dto = new DrugDto();
            dto.setDrugName(entity.getDrugName());
            dto.setPrice(entity.getPrice());
            dto.setId(entity.getId());
            dto.setQuantity(entity.getQuantity());
            dtoList.add(dto);
        }
        return dtoList;
    }
}
