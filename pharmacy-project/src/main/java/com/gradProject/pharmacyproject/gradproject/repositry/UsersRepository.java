package com.gradProject.pharmacyproject.gradproject.repositry;

import com.gradProject.pharmacyproject.gradproject.dto.UserDto;
import com.gradProject.pharmacyproject.gradproject.entities.implments.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UsersRepository extends JpaRepository<UserEntity, Integer> {
     UserDto findByUserNameAndPassword(String name, String password);

    UserDto findUserByUserName(String username);
}
