package com.gradProject.pharmacyproject.gradproject.dto;


import com.gradProject.pharmacyproject.gradproject.entities.implments.UserEntity;

public class UserDto {
    private int id;

    private String userName;

    private String password;

    private String email;

    private boolean isAdmin;

    public UserDto() {
    }

    public UserDto(UserEntity userEntity) {
        this.id = userEntity.getId();
        this.password = userEntity.getPassword();
        this.userName = userEntity.getUserName();
        this.isAdmin = userEntity.isAdmin();

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
