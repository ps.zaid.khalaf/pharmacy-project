package com.gradProject.pharmacyproject.gradproject.controllers;

import com.gradProject.pharmacyproject.gradproject.dto.CreditDto;
import com.gradProject.pharmacyproject.gradproject.services.CreditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/credit")
public class CreditController {
    @Autowired
    CreditService creditService;

    @PostMapping
    public ResponseEntity<String> addCredit(@RequestBody CreditDto creditDto) {
        return creditService.addCredit(creditDto);
    }

    @DeleteMapping("/delete/{email}")
    @Transactional
    public ResponseEntity<String> deleteCredit(@PathVariable(value = "email") String email) {
        if (!creditService.isExist(email)) {
            return ResponseEntity.badRequest().body("no credit info found");
        }
        return creditService.deleteCredit(email);
    }
}
