package com.gradProject.pharmacyproject.gradproject.security;

import com.gradProject.pharmacyproject.gradproject.dto.UserDto;
import com.gradProject.pharmacyproject.gradproject.repositry.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Optional;

@Component
public class JwtUserDetailsService implements UserDetailsService {
    @Autowired
    private UsersRepository repository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserDto userDto = repository.findUserByUserName(username);
        if (userDto == null) {
            throw new UsernameNotFoundException("SysUser not found with username: " + username);

        } else {
            return new org.springframework.security.core.userdetails.User(userDto.getUserName(), userDto.getPassword(),
                    new ArrayList<>());
        }
    }

    public Optional<User> getCurrentUser() {
        org.springframework.security.core.userdetails.User principal = (org.springframework.security.core.userdetails.User) SecurityContextHolder.
                getContext().getAuthentication().getPrincipal();
        return Optional.of(principal);
    }

}
