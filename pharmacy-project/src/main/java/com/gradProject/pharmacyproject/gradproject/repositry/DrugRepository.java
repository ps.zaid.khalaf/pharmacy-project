package com.gradProject.pharmacyproject.gradproject.repositry;

import com.gradProject.pharmacyproject.gradproject.entities.implments.DrugEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface DrugRepository extends JpaRepository<DrugEntity, Integer> {
DrugEntity getByDrugName(String drugName);
Optional<DrugEntity> findByDrugName(String drugName);
void deleteByDrugName(String drugName);

}
