package com.gradProject.pharmacyproject.gradproject.services;

import com.gradProject.pharmacyproject.gradproject.dto.UserDto;
import com.gradProject.pharmacyproject.gradproject.entities.implments.UserEntity;
import com.gradProject.pharmacyproject.gradproject.mapping.UserMapping;
import com.gradProject.pharmacyproject.gradproject.repositry.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class UserService {
    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    PasswordEncoder bcryptEncoder;

    public ResponseEntity createUser(UserEntity entity) {
        if (entity.getUserName().isEmpty()) {
            throw new IllegalArgumentException("error accord");
        }
        entity.setPassword(bcryptEncoder.encode(entity.getPassword()));
        usersRepository.save(entity);
        return new ResponseEntity(HttpStatus.OK);
    }

    public boolean isRegistered(UserDto userDto) {
        UserDto dto = usersRepository.findByUserNameAndPassword(userDto.getUserName(), userDto.getPassword());
        if (dto == null) {
            return false;
        }
        return !dto.getUserName().isEmpty() && !dto.getPassword().isEmpty();

    }

    public ResponseEntity<UserDto> getUserByName(String userName) {
        UserDto user = usersRepository.findUserByUserName(userName);
        if (user == null) {
            return ResponseEntity.badRequest().build();
        }
        return ResponseEntity.ok().body(user);
    }

    public List<UserDto> getAllUsers() {
        List<UserDto> userList = UserMapping.fromEntityListToDtoList(usersRepository.findAll());
        if (userList.isEmpty()) {
            return null;
        }
        return userList;
    }

    public ResponseEntity deleteUser(String userName) {
        UserDto user = usersRepository.findUserByUserName(userName);
        if (Objects.isNull(user)) {
            return ResponseEntity.badRequest().build();
        }
        usersRepository.delete(UserMapping.fromDtoToEntity(user));
        return ResponseEntity.ok().build();
    }

}
