package com.gradProject.pharmacyproject.gradproject.mapping;

import com.gradProject.pharmacyproject.gradproject.dto.DrugDto;
import com.gradProject.pharmacyproject.gradproject.dto.UserDto;
import com.gradProject.pharmacyproject.gradproject.entities.implments.DrugEntity;
import com.gradProject.pharmacyproject.gradproject.entities.implments.UserEntity;

import java.util.ArrayList;
import java.util.List;

public class UserMapping {

    public static UserDto fromEntityToDto(UserEntity userEntity) {
        UserDto userDto = new UserDto();
        userDto.setId(userEntity.getId());
        userDto.setUserName(userEntity.getUserName());
        userDto.setPassword(userEntity.getPassword());
        userDto.setEmail(userEntity.getEmail());
        userDto.setAdmin(userEntity.isAdmin());
        return userDto;
    }

    public static UserEntity fromDtoToEntity(UserDto userDto) {
        return new UserEntity(userDto);
    }

    public static List<UserDto> fromEntityListToDtoList(List<UserEntity> entities) {
        List<UserDto> dtoList = new ArrayList<>();
        for (UserEntity entity : entities) {
            UserDto dto = new UserDto();
            dto.setUserName(entity.getUserName());
            dto.setEmail(entity.getEmail());
            dto.setAdmin(entity.isAdmin());
            dto.setPassword(entity.getPassword());
            dtoList.add(dto);
        }
        return dtoList;
    }


}
