package com.gradProject.pharmacyproject.gradproject.entities.implments;

import com.gradProject.pharmacyproject.gradproject.dto.UserDto;

import javax.persistence.*;

@Table(name = "user")
@Entity
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @Column(name = "user_name")
    private String userName;
    @Column(name = "password")
    private String password;
    @Column(name = "email")
    private String email;
    @Column(name = "is_admin")
    private boolean isAdmin;

    public UserEntity() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UserEntity(UserDto userDto) {
        this.id = userDto.getId();
        this.password = userDto.getPassword();
        this.userName = userDto.getUserName();
        this.email = userDto.getEmail();
        this.isAdmin = userDto.isAdmin();
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }
}
